import { PrimediaRealtimePage } from './app.po';

describe('primedia-realtime App', function() {
  let page: PrimediaRealtimePage;

  beforeEach(() => {
    page = new PrimediaRealtimePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
