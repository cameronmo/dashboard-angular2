export interface ISiteUsage {
    id: number;
    name: string;
    url: string;
    active_users: number;
    detail: Array<IDetail>;
}

export interface IDetail {
    id: number;
    rtm_ga_id: number;
    path: string;
    active_users: number;
    abs_path: string;
    query: any;
    entity_id: number;
    entity_type: any;
    delta: any;
}
