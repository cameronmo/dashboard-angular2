import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import 'rxjs/add/observable/of';

@Injectable()
export class ApiCallService {

    // misc
    people$: Observable<any>;
    private apiUrl = 'http://10.1.127.67:8919/rtm_ga_data/harvest_rtm.json'

    constructor(protected http: Http) {
    };

    getAll(): Observable<any> {
        this.people$ = this.http
            .get(this.apiUrl)
        return this.people$;
    }

    getData() {
        return this.http.get(this.apiUrl)
                .map((res) => res.json());
    }
}
