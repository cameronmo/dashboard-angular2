import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
    menu_links: Array<any>;

    constructor() {
    }

    ngOnInit() {
        this.menu_links = [
            {
                name: 'Realtime',
                url: '/'
            },
            {
                name: 'Realtime Graph',
                url: '/'
            },
            {
                name: 'GA Reports',
                url: '/'
            },
            {
                name: 'Content',
                url: '/'
            }
        ];
    }
}
