import {Component, OnDestroy, OnInit} from '@angular/core';
import { ApiCallService } from '../../services/api-call/api-call.service';
import { AnonymousSubscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Component({
  selector: 'app-live-users',
  templateUrl: './live-users.component.html',
  styleUrls: ['./live-users.component.scss']
})
export class LiveUsersComponent implements OnInit {
    sites_data: Array<any>;
    sites_users: Array<number> = [];
    sites_names: Array<string> = [];
    total_users: number;
    private timerSubscription: AnonymousSubscription;
    private postsSubscription: AnonymousSubscription;

  constructor(protected api_service: ApiCallService) {

  }

  ngOnInit() {
      this.refreshData();
  }

  private subscibeToData(): void {
    this.timerSubscription = Observable.timer(40000)
        .subscribe(() => this.refreshData());
  }

  private refreshData(): void {
      this.postsSubscription = this.api_service.getData()
          .subscribe(
              (data) => {
                    this.sites_data = data;
                    this.subscibeToData();
                    this.plotLiveUsers(this.sites_data);
                },
              error => {
                console.log(error);
              },
              () => {
                  console.log('completed');
              }
          );
  };

  plotLiveUsers(data) {
      this.sites_names = [];
      this.sites_users = [];
      for (let i = 0; i < data.sites.length; i++) {
          this.sites_names.push(data.sites[i].name);
          this.sites_users.push(data.sites[i].active_users);
      }

      this.total_users = this.sites_users.reduce((a, b) => a + b, 0);

  };
}
