import {Component, OnInit} from '@angular/core';

// api
import {ApiCallService} from '../../services/api-call/api-call.service';

// rx.js
import { AnonymousSubscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

// charts
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';


@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent implements OnInit {

  sites_data: Array<any>;
  sites_users: Array<number> = [];
  sites_names: Array<string> = [];
  graph_data: Array<Object> = [];
  private timerSubscription: AnonymousSubscription;
  private postsSubscription: AnonymousSubscription;

  constructor(protected api_service: ApiCallService) {
  }
  ngOnInit() {
      this.refreshData();
  }

  private subscibeToData(): void {
        this.timerSubscription = Observable.timer(40000)
            .subscribe(() => this.refreshData());
    }
  private refreshData(): void {
        this.postsSubscription = this.api_service.getData()
            .subscribe(
                (data) => {
                    this.sites_data = data;
                    this.subscibeToData();
                    this.plotPieChart(this.sites_data);
                },
                error => {
                    console.log(error);
                },
                () => {
                    console.log('completed');
                }
            );
    };
  plotPieChart(data) {
        this.sites_names = [];
        this.sites_users = [];
        this.graph_data = [];
        for (let i = 0; i < data.sites.length; i++) {
          this.graph_data.push({
            name: data.sites[i].name,
            value: data.sites[i].active_users,
          });
        }
      console.log(this.graph_data);
    };
}
