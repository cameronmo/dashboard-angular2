import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { MenuComponent } from './components/menu/menu.component';
import { ApiCallService } from './services/api-call/api-call.service';
import { PieChartComponent } from './components/pie-chart/pie-chart.component';
import {ChartsModule} from 'ng2-charts';
import { LiveUsersComponent } from './components/live-users/live-users.component';
import { TopStoriesComponent } from './components/top-stories/top-stories.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    PieChartComponent,
    LiveUsersComponent,
    TopStoriesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ChartsModule,
    BrowserAnimationsModule
  ],
  providers: [ ApiCallService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
